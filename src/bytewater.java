package me.matt.bytewater;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ByteWater
        extends JavaPlugin
        implements Listener {
    public void onEnable() {
        this.saveDefaultConfig();
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + this.getConfig().getString("enabeled"))));
        Bukkit.getServer().getPluginManager().registerEvents((Listener)this, (Plugin)this);
    }

    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + this.getConfig().getString("disabeled"))));
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (this.getConfig().getBoolean("damage")) {
            Player p = e.getPlayer();
            Location l = e.getTo();
            if (!(p.hasPermission("bw.nodamage") || l.getBlock().getType() != Material.WATER && l.getBlock().getType() != Material.STATIONARY_WATER)) {
                p.damage(3.0);
                p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + this.getConfig().getString("goOutDamage"))));
            }
        }
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase(bw) && args.length == 0) {
            p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + "\u00a77Help")));
            p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + "\u00a77Plugin by Daxxiee")));
            p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + "\u00a77Commands:")));
            p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)"\u00a75/bw \u00a78>> \u00a77Shows this Page"));
            p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)"\u00a75/bw reload \u00a78>> \u00a77Reloads the Config"));
            return true;
        }
        if (args[0].equalsIgnoreCase("reload") && args.length == 1) {
            if (p.hasPermission("bw.reload")) {
                this.reloadConfig();
                p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + this.getConfig().getString("configreload"))));
                return true;
            }
            p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(this.getConfig().getString("prefix")) + this.getConfig().getString("noPermission"))));
            return true;
        }
        return false;
    }
}
